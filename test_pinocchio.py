import pinocchio
from sys import argv
from os.path import dirname, join, abspath
from sympy.utilities.autowrap import autowrap

import utils.filemanager as fm
import utils.statesFormat as sf
import optimize.costFunction as cf
import utils.display as disp
import optimize.polyCOD as pcod

import numpy as np
import math as m
# This path refers to Pinocchio source code but you can define your own directory here.
#pinocchio_model_dir = join(dirname(dirname(str(abspath(__file__)))), "models")

# You should change here to set up your own URDF file or just pass it as an argument of this example.
#urdf_filename = pinocchio_model_dir + '/example-robot-data/robots/ur_description/urdf/ur5_robot.urdf' if len(argv)<2 else argv[1]

urdf_filename = 'model/2R.urdf' if len(argv)<2 else argv[1]

# Load the urdf model
model = pinocchio.buildModelFromUrdf(urdf_filename)
print('model name: ' + model.name)

# Create data required by the algorithms
data = model.createData()

# Sample a random configuration
#q = pinocchio.randomConfiguration(model)

#x,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read("res/simple_cod_T101_w2_026.csv")
nz = 6;	nu = 0
Sstart = [0,0,0,0,0,0];	Sgoal = [120*m.pi/180,40*m.pi/180,0,0,0,0]
myMod = [1.0,1.0,1.0,1.0,0.5,0.5,0.5,0.5]
w = [0.6,0.4]
w /= np.linalg.norm(w)
nc = len(w)
degree = 6
T = 101
trueT = 1; step = 0.01
#myarg, x = pcod.polyCOD_tau(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,"lucas")
x,myarg,degree,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read("res/poly_cod_T101_w2_lucas.csv")
xt = x.reshape((T,nz))
tau = np.zeros((T,2))
for i in range(T-5):
	q = xt[i,0:2]
	v = xt[i,2:4]
	a = xt[i,4:6]
	pinocchio.forwardKinematics(model,data,q)
	tau[i,:] = pinocchio.rnea(model,data,q,v,a)
#print(tau)

funTausy = cf.symb2function(cf.symb_tau(),myMod)
myfunc = autowrap(funTausy)
tau2,dtau,ddtau = sf.tau_eval(x, nz, T, myfunc)

#print(tau2)

alltau = np.concatenate((tau,tau2),axis=1)

myT = np.arange(0,1+0.01,0.01)

disp.simple_figure(myT,alltau,"tau_pinocchio")

tmp = np.square(np.subtract(tau[:,0],tau2[:,0]))
res = m.sqrt(np.sum(tmp)/len(tau))
print("RMSE tau1: {0}".format(res))
tmp = np.square(np.subtract(tau[:,1],tau2[:,1]))
res = m.sqrt(np.sum(tmp)/len(tau))
print("RMSE tau2: {0}".format(res))


print("Test matrices :")
j = 99
q = xt[j,0:2]
v = xt[j,2:4]
a = xt[j,4:6]

print("\n Q = {0} \n V = {1} \n A = {2} \n".format(q,v,a))

pinocchio.forwardKinematics(model,data,q,v)
mytau = pinocchio.rnea(model,data,q,v,a)
pinocchio.computeRNEADerivatives(model,data,q,v,a)
pinocchio.computeGeneralizedGravity(model,data,q)
pinocchio.computeCoriolisMatrix(model,data,q,v)


m1 = myMod[0]
m2 = myMod[1]
l1 = myMod[2]
l2 = myMod[3]
r1 = myMod[4]
r2 = myMod[5]
I1 = myMod[6]
I2 = myMod[7]
g = 9.81
a1 = (m1*r1**2)+(m2*(l1**2 + r2**2))+I1+I2
a2 = m2*l1*r2
a3 = (m2*r2**2)+I2
b1 = (l1*m2)+(r1*m1)
b2 = r2*m2

M =np.array([[a1+(2*a2*np.cos(q[1])), a3+(a2*np.cos(q[1]))],[a3+(a2*np.cos(q[1])), a3]])
C =np.array([[-a2*v[1]*np.sin(q[1]), -a2*(v[0]+v[1])*np.sin(q[1])],[a2*v[0]*np.sin(q[1]), 0]])
G =np.array([[(b1*g*np.cos(q[0]))+(b2*g*np.cos(q[0] + q[1]))],[b2*g*np.cos(q[0] + q[1])]])

J = np.array([[-l1*m.sin(q[0]), 0],
	[l1*m.cos(q[0]) , 0],
	[0 , 0] ,
	[-l1*m.sin(q[0]) - l2*m.sin(q[0]+q[1]) , - l2*m.sin(q[0]+q[1])],
	[l1*m.cos(q[0]) + l2*m.cos(q[0]+q[1]) , l2*m.cos(q[0]+q[1])],
	[0 , 0]])

print("tau :")
print(mytau)
print(tau2[j,:])
print("G :")
print(data.g)
print(np.transpose(G))
print("C :")
print(data.C)
print(C)
print("M :")
print(data.M)
print(M)

pinocchio.crba(model,data,q)
print("CRBA :\n{0}".format(data.M))
print("End test matrices")

print("Jacobienne : \n{0}".format(data.J))
print("My Jacobienne : \n{0}".format(J))
J = pinocchio.computeJointJacobian(model,data,q,2)
print("Jacobienne bis : \n{0}".format(J))

pinocchio.updateFramePlacements(model,data)
pinocchio.computeJointJacobians(model,data,q)
pinocchio.framesForwardKinematics(model,data,q)
print(model.getFrameId('link_3'))
print(pinocchio.getFrameJacobian(model,data,model.getFrameId('link_3'),pinocchio.ReferenceFrame(0)))
print("Number of joints " , model.nv)

# Print out the placement of each joint of the kinematic tree
for name, oMi in zip(model.names, data.oMi):
    print(("{:<24} : {: .2f} {: .2f} {: .2f}"
          .format( name, *oMi.translation.T.flat )))
