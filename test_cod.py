import sys
import time

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import utils.polynomialFunction as pl

import optimize.simpleCOD as cod
import optimize.simpleCOI as coi
import optimize.polyCOD as pcod
import optimize.polyCOI as pcoi
import optimize.polyCODtrunc as ptcod
import optimize.polyCOItrunc as ptcoi
import optimize.costFunction as cf

import math as m
import numpy as np
import sympy as sy
import scipy.linalg as sci
import scipy.integrate as integrate

from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt

def print_time_elapsed(start):
	end = time.time()
	hours, rem = divmod(end-start, 3600)
	minutes, seconds = divmod(rem, 60)
	print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
	return

def rmse(a,b):
	tmp = np.square(np.subtract(a,b))
	res = m.sqrt(np.sum(tmp)/len(a))
	return res

def mydeltapolynome(delta):
	f_delta = lambda x: delta*((-x**3)+(3.0*x**4)+(-3.0*x**5)+(x**6))
	y, err = integrate.quad(f_delta, 0, 1)
	return y

# Complete test :
def simple_cod(nz,T,Sstart,Sgoal,w,myMod,saveName):
	start = time.time()
	x = cod.simpleCOD_tau(nz,T,Sstart,Sgoal,w,myMod,saveName)
	print_time_elapsed(start)
	return x

def simple_coi(x,nz,nc,T,myMod):
	start = time.time()
	new_z,new_w = coi.simpleCOI_tau(x,nz,nc,T,myMod)
	print_time_elapsed(start)
	return new_w

def poly_cod(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName):
	start = time.time()
	myarg, realx = pcod.polyCOD_tau(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
	print_time_elapsed(start)
	return myarg,realx

def poly_cod_trunc(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName):
	start = time.time()
	myarg, realx, fullarg = ptcod.polyCODtrunc_tau(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
	print_time_elapsed(start)
	return myarg,realx, fullarg

def poly_coi(myarg,degree,nz,nc,trueT,step,myMod):
	start = time.time()
	new_z,new_w = pcoi.polyCOI_tau(myarg,degree,nz,nc,trueT,step,myMod)
	print_time_elapsed(start)
	return new_w

def poly_coi_trunc(myarg,x,degree,nz,nc,trueT,step,myMod):
	start = time.time()
	new_w,sL = ptcoi.polyCOItrunc_tau(myarg,x,degree,nz,nc,trueT,step,myMod)
	print_time_elapsed(start)
	return new_w,sL


# Initialize functions for test loops
def init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod):
	funTau = autowrap(cf.function4poly(cf.symb_tau(),myMod,degree,2,nz,trueT,Sstart,Sgoal))
	print("Autowrap done")
	return funTau

def init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod):
	funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
	print("autowrap done")
	return funGradTau

def init_coi(nz,trueT,degree,Sstart,Sgoal,myMod):
	funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,1,degree))
	print("autowrap done")
	return funGradTau

def optimal_points(nz,trueT,step,Sstart,Sgoal,degree,funcod,filename,optsteps=1):
	# Take between 1h30 and 2h30
	myfunw = lambda x: [np.cos(x),np.sin(x)]
	start = time.time()
	totang = np.arange(1,90,optsteps)
	xdata = np.zeros(len(totang))
	ydata = np.zeros(len(totang))
	valuesW = np.zeros((len(totang),2))
	for i in range(len(totang)):
		print('_________')
		count = "Cod states : {0} / {1} ".format(i+1,len(totang))
		print(count)
		ang = totang[i]*m.pi/(2*90)
		w = myfunw(ang)
		w = w / np.linalg.norm(w)
		if i==0:
			ptarg = ptcod.cod_optim(w,nz,trueT,step,Sstart,Sgoal,degree,funcod)
		else:
			ptarg = ptcod.cod_optim(w,nz,trueT,step,Sstart,Sgoal,degree,funcod,ptarg)
		xdata[i] = ptarg[0]
		ydata[i] = ptarg[1]
		valuesW[i,:] = w
	print_time_elapsed(start)
	fm.save_matrices(ydata,xdata,valuesW,"tot_w_{0}".format(filename))

def main(myarg):
	if len(myarg)==1:
		print("***********************")
		print("At least one argument is missing:")
		print("> python3 test_cod mode [filename]")
		print("mode: int \n 1: simple COD,\n 2: poly COD,\n 3: simple COD-polyCOI,\n 4: reading file)")
		print("***********************")
		return 0
	typeOpt=int(myarg[1])
	nz = 6;	nu = 0
	T = 101
	Sstart = [0,0,0,0,0,0];	Sgoal = [120*m.pi/180,40*m.pi/180,0,0,0,0]
	#myMod = [1,1.5,1,1.2,0.5,0.6,0.5,0.7]
	myMod = [1,1,1,1,0.5,0.5,0.5,0.5]
	w = [0.6,0.4]
	nc = len(w)
	degree = 6
	trueT = 1; step = 0.01

	if typeOpt == 1: #COD + COI simple
		saveName = "100"
		print(w / np.linalg.norm(w))
		x = simple_cod(nz,T,Sstart,Sgoal,w,myMod,saveName)
		#noiseSample = np.random.normal(0, 0, size=len(x))
		#x = x + noiseSample
		#x,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read("res/simple_cod_T101_w2_001.csv")
		new_w = simple_coi(x,nz,nc,T,myMod)
	elif typeOpt == 2: # poly cod/coi
		saveName = "011"
		myarg,realx = poly_cod(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
		print(myarg)
		new_w = poly_coi(myarg,degree,nz,nc,trueT,step,myMod)
	elif typeOpt == 3: #poly trunc cod/coi
		saveName = "011"
		myarg,realx, fullarg = poly_cod_trunc(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
		new_w = poly_coi_trunc(myarg,realx,degree,nz,nc,trueT,step,myMod)
	elif typeOpt == 4: # figure david
		funcod = init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
		funcoi = init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)

		# FIGURE DAVID
		xdata,ydata,valuesW = fm.read_matrices("res/matrices_tot_w.csv")

		T=len(np.arange(0,trueT+step,step))
		alphas = np.arange(45,75,0.1)#np.min(xdata)-10, np.max(xdata)+10, 0.05)
		betas = np.arange(-180,-90,0.5)#np.arange(np.min(ydata)-5,np.max(ydata)+5, 0.05)
		MatrixS = np.zeros((len(betas),len(alphas)))
		MatrixW = np.zeros((len(betas),len(alphas),2))
		for i in range(len(alphas)):
			count = "states : {0} / {1} ".format(i+1,len(alphas))
			print(count)
			for j in range(len(betas)):
				a = alphas[i]
				b = betas[j]
				J = ptcoi.my_coi_matrix_trunc(np.array([a,b]),degree,nz,nc,trueT,step,myMod,funcoi)
				J = J / np.linalg.norm(J)
				s = sci.svdvals(J)
				s = s / np.linalg.norm(s)
				MatrixS[j][i] = s[-1]
				new_z,new_w = pcoi.my_resolution(J,nc)
				MatrixW[j,i,:] = new_w


		myargfunc = [[45.,-102.,48.51456696,-107.70283773],[55.,-102.,52.16062037,-98.19999742],[65.,-102.,60.26594755,-96.5445664],[57.0,-155.0,55.133,-151.0399]]
		pa = np.zeros(len(myargfunc))
		pb = np.zeros(len(myargfunc))
		for i in range(len(myargfunc)):
			pa[i] = (myargfunc[i][1]-myargfunc[i][3])/(myargfunc[i][0]-myargfunc[i][2])
			pb[i] = myargfunc[i][3] - pa[i]*myargfunc[i][2]

		# L1
		#pa = (-155.0+151.0399)/(57.0-55.133)
		#pb = -151.0399 - pa*55.133
		#pf = lambda x: pa*x + pb
		test =np.arange(45.0,75.0,5) #np.arange(45,65,2)
		mypointw = np.zeros((len(test),2,len(myargfunc)))
		mypoint = np.zeros((len(test),4,len(myargfunc)))
		for t in range(len(myargfunc)):
			print("------------- new line")
			pf = lambda x: pa[t]*x + pb[t]
			for k in range(len(test)):
				count = "line : {0} / {1} ".format(k+1,len(test))
				print(count)
				a = test[k]
				b = pf(test[k])
				print(np.array([a,b]))
				J = ptcoi.my_coi_matrix_trunc(np.array([a,b]),degree,nz,nc,trueT,step,myMod,funcoi)
				J = J / np.linalg.norm(J)
				s = sci.svdvals(J)
				s = s / np.linalg.norm(s)
				new_z,new_w = pcoi.my_resolution(J,nc)
				ptarg = ptcod.cod_optim(new_w,nz,trueT,step,Sstart,Sgoal,degree,funcod)
				mypointw[k,:,t] = new_w
				mypoint[k,:,t] = [a, b, ptarg[0], ptarg[1]]

		print(mypointw)
		print(mypoint)
		print(np.shape(MatrixS))
		print(np.max(MatrixS))
		print(np.min(MatrixS))
		disp.display_matrix(MatrixS,mypoint,mypointw,alphas,betas,ydata,xdata,valuesW,"test_tot_new")
	elif typeOpt == 6: # non optimize trajectoy special ?
		funcod = init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
		funcoi = init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
		funcod_simple = autowrap(cod.symb_tauSubs(myMod))
		T=len(np.arange(0,trueT+step,step))
		#ydata = np.zeros((nz*T,4)) #traj
		ydata = np.zeros((T,2,4)) #tau

		a = float(70)
		b = float(-142)
		trajX,args = pl.eval_poly_trunc(np.array([a,b]),nz,T,trueT,step,Sstart,Sgoal,degree)
		#ydata[:,0] = trajX
		tau,dtau,ddtau = cod.dTau(trajX,T,funcod_simple)
		ydata[:,:,0] = tau
		J = ptcoi.my_coi_matrix_trunc(np.array([a,b]),degree,nz,nc,trueT,step,myMod,funcoi)
		J = J / np.linalg.norm(J)
		s = sci.svdvals(J)
		s = s / np.linalg.norm(s)
		print('sL :')
		print(s[-1])
		new_z,new_w = pcoi.my_resolution(J,nc)
		print("w :")
		print(new_w)
		ptarg = ptcod.cod_optim(new_w,nz,trueT,step,Sstart,Sgoal,degree,funcod)
		print("Poly :")
		print(np.array([a,b]))
		print(ptarg)
		trajX,args = pl.eval_poly_trunc(ptarg,nz,T,trueT,step,Sstart,Sgoal,degree)
		#ydata[:,1] = trajX
		tau,dtau,ddtau = cod.dTau(trajX,T,funcod_simple)
		ydata[:,:,1] = tau

		print('NEWS ======')
		"""alpha = b-ptarg[1] / (a - ptarg[0])
		beta = (ptarg[1]*a - ptarg[0]*b) / (a - ptarg[0])

		J = ptcoi.my_coi_matrix_trunc(np.array([60,alpha*60 + beta]),degree,nz,nc,trueT,step,myMod,funcoi)"""
		new_ptarg = np.array([float(70),float(-122)])
		J = ptcoi.my_coi_matrix_trunc(new_ptarg,degree,nz,nc,trueT,step,myMod,funcoi)
		J = J / np.linalg.norm(J)
		s = sci.svdvals(J)
		s = s / np.linalg.norm(s)
		print('sL :')
		print(s[-1])
		new_z,new_w = pcoi.my_resolution(J,nc)
		print("w :")
		print(new_w)
		ptarg = ptcod.cod_optim(new_w,nz,trueT,step,Sstart,Sgoal,degree,funcod)
		print("Poly :")
		print(new_ptarg)
		print(ptarg)
		trajX,args = pl.eval_poly_trunc(new_ptarg,nz,T,trueT,step,Sstart,Sgoal,degree)
		#ydata[:,2] = trajX
		tau,dtau,ddtau = cod.dTau(trajX,T,funcod_simple)
		ydata[:,:,2] = tau
		trajX,args = pl.eval_poly_trunc(ptarg,nz,T,trueT,step,Sstart,Sgoal,degree)
		#ydata[:,3] = trajX
		tau,dtau,ddtau = cod.dTau(trajX,T,funcod_simple)
		ydata[:,:,3] = tau

		#disp.display_trajectory(ydata,T,nz,'fig/traj_same_proj3.png')
		disp.display_torque(ydata,T,nz,'fig/tau_same_proj2.png')
	elif typeOpt == 7:
		funcoi = init_coi(nz,trueT,degree,Sstart,Sgoal,myMod)

		ydata = np.zeros((nz*T,3)) #traj

		#saveName = "100"
		#x = simple_cod(nz,T,Sstart,Sgoal,w,myMod,saveName)
		x,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read('res/simple_cod_T101_w2_100.csv')
		ydata[:,0] = x
		parg = pl.mypolynome(x,nz,T,trueT,step,degree)
		trajX = pcod.eval_poly(parg,nz,T,trueT,step,degree)
		ydata[:,1] = trajX
		print(rmse(trajX,x))
		print('Polynome:')
		print(pl.complete2trunc(parg))
		J = pcoi.my_coi_matrix(parg,degree,nz,nc,trueT,step,myMod,funcoi)
		J = J / np.linalg.norm(J)
		s = sci.svdvals(J)
		s = s / np.linalg.norm(s)
		print('sL :')
		print(s)
		new_z,new_w = pcoi.my_resolution(J,nc)
		parg, trajX2 = pcod.poly_cod(nz,trueT,step,degree,Sstart,Sgoal,new_w,myMod,None)
		ydata[:,2] = trajX2
		print(rmse(trajX2,x))
		print('Polynome:')
		print(pl.complete2trunc(parg))

		disp.display_trajectory(ydata,T,nz,'fig/interp_traj_simple2.png')
	elif typeOpt == 8: # all projections tests
		ydata = np.zeros((nz*T,6)) #traj
		funcod = init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
		funcoi = init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)

		T=len(np.arange(0,trueT+step,step))
		alphas = np.arange(30,90,0.5)#np.min(xdata)-10, np.max(xdata)+10, 0.05)
		betas = np.arange(-200,-80,1)#np.arange(np.min(ydata)-5,np.max(ydata)+5, 0.05)
		MatrixS = np.zeros((len(betas),len(alphas)))
		MatrixW = np.zeros((len(betas),len(alphas),2))
		for i in range(len(alphas)):
			count = "states : {0} / {1} ".format(i+1,len(alphas))
			print(count)
			for j in range(len(betas)):
				a = alphas[i]
				b = betas[j]
				J = ptcoi.my_coi_matrix_trunc(np.array([a,b]),degree,nz,nc,trueT,step,myMod,funcoi)
				J = J / np.linalg.norm(J)
				s = sci.svdvals(J)
				s = s / np.linalg.norm(s)
				MatrixS[j][i] = s[-1]
				new_z,new_w = pcoi.my_resolution(J,nc)
				MatrixW[j,i,:] = new_w


		some_w = np.array([[0.9,0.1],[0.6,0.4],[0.1,0.9]])
		noiseSample = np.random.normal(0, 0.05, size=nz*T)
		for i in range(3):
			mypointw = np.zeros((1,2,5))
			mypoint = np.zeros((1,4,5))
			print("_________________{0}______________________".format(i))
			w = some_w[i,:]
			# Trajectory Initialisation
			print('******* Initialisation *********')
			print("w : {0}".format(w / np.linalg.norm(w)))
			arg_init = ptcod.cod_optim(w / np.linalg.norm(w),nz,trueT,step,Sstart,Sgoal,degree,funcod)
			trajX_init,args = pl.eval_poly_trunc(arg_init,nz,T,trueT,step,Sstart,Sgoal,degree)
			ydata[:,0] = trajX_init
			print("Alpha/Beta : {0}".format(arg_init))

			# Noised trajectory
			print('******* Noised *********')
			trajX_noised = trajX_init + noiseSample
			arg_noised = pl.complete2trunc(pl.mypolynome(trajX_noised,nz,T,trueT,step,degree))
			ydata[:,1] = trajX_noised
			print("Alpha/Beta : {0}".format(arg_noised))

			print("RMSE noised : {0} ".format(rmse(trajX_init,trajX_noised)))
			mypointw[0,:,0] = w
			mypoint[0,:,0] = [arg_noised[0], arg_noised[1], arg_init[0], arg_init[1]]


			#Projection IOC
			print('******* IOC Projection *********')
			J = ptcoi.my_coi_matrix_trunc(arg_noised,degree,nz,nc,trueT,step,myMod,funcoi)
			J = J / np.linalg.norm(J)
			s = sci.svdvals(J)
			s = s / np.linalg.norm(s)
			print("sL : {0}".format(s[-1]))
			new_z,new_w = pcoi.my_resolution(J,nc)
			print("w : {0}".format(new_w))
			arg_proj_ioc = ptcod.cod_optim(new_w,nz,trueT,step,Sstart,Sgoal,degree,funcod)

			trajX_proj_ioc,args = pl.eval_poly_trunc(arg_proj_ioc,nz,T,trueT,step,Sstart,Sgoal,degree)
			ydata[:,2] = trajX_proj_ioc
			if new_w[0] < 0 or new_w[1] < 0:
				print('!!! WARNING !!! ')
				mypointw[0,:,1] = np.array([0,0])
				mypoint[0,:,1] = [arg_init[0], arg_init[1], arg_init[0], arg_init[1]]
			else:
				mypointw[0,:,1] = new_w
				mypoint[0,:,1] = [arg_proj_ioc[0], arg_proj_ioc[1], arg_init[0], arg_init[1]]
			print("Alpha/Beta : {0}".format(arg_proj_ioc))
			print("RMSE proj ioc : {0} ".format(rmse(trajX_init,trajX_proj_ioc)))

			# Axial projections
			print('******* Axial Projection *********')
			alphasx,betasy,valuesW = fm.read_matrices("res/matrices_tot_w.csv")

			# alpha
			print('----- Alpha')
			print(alphasx)
			diff_alph = alphasx - arg_noised[0]
			print(diff_alph)
			ida = np.argmin(np.abs(diff_alph))
			print(ida)
			w_a = valuesW[ida,:]
			print("w : {0}".format(w_a))
			arg_proj_a = ptcod.cod_optim(w_a,nz,trueT,step,Sstart,Sgoal,degree,funcod)
			trajX_proj_a,args = pl.eval_poly_trunc(arg_proj_a,nz,T,trueT,step,Sstart,Sgoal,degree)
			ydata[:,3] = trajX_proj_a
			print("Alpha/Beta : {0}".format(arg_proj_a))
			print("RMSE proj a : {0} ".format(rmse(trajX_init,trajX_proj_a)))
			mypointw[0,:,2] = w_a
			mypoint[0,:,2] = [arg_proj_a[0], arg_proj_a[1], arg_init[0], arg_init[1]]

			# beta
			print('----- Beta')
			print(betasy)
			diff_bet = betasy - arg_noised[1]
			print(diff_bet)
			idb = np.argmin(np.abs(diff_bet))
			print(idb)
			w_b = valuesW[idb,:]
			print("w : {0}".format(w_b))
			arg_proj_b = ptcod.cod_optim(w_b,nz,trueT,step,Sstart,Sgoal,degree,funcod)
			trajX_proj_b,args = pl.eval_poly_trunc(arg_proj_b,nz,T,trueT,step,Sstart,Sgoal,degree)
			ydata[:,4] = trajX_proj_b
			print("Alpha/Beta : {0}".format(arg_proj_b))
			print("RMSE proj b : {0} ".format(rmse(trajX_init,trajX_proj_b)))
			mypointw[0,:,3] = w_b
			mypoint[0,:,3] = [arg_proj_b[0], arg_proj_b[1], arg_init[0], arg_init[1]]

			print('----- Alpha and Beta')
			idab = np.argmin((np.abs(diff_bet)+np.abs(diff_alph))/2.0)
			w_ab = valuesW[idab,:]
			print("w : {0}".format(w_ab))
			arg_proj_ab = ptcod.cod_optim(w_ab,nz,trueT,step,Sstart,Sgoal,degree,funcod)
			trajX_proj_ab,args = pl.eval_poly_trunc(arg_proj_ab,nz,T,trueT,step,Sstart,Sgoal,degree)
			ydata[:,5] = trajX_proj_ab
			print("Alpha/Beta : {0}".format(arg_proj_ab))
			print("RMSE proj ab : {0} ".format(rmse(trajX_init,trajX_proj_ab)))
			mypointw[0,:,4] = w_ab
			mypoint[0,:,4] = [arg_proj_ab[0], arg_proj_ab[1], arg_init[0], arg_init[1]]

			disp.display_trajectory(ydata[:,0:4],T,nz,"fig/comparison{0}3_projection2-3.png".format(i))
			disp.display_trajectory(ydata[:,[0,1,4,5]],T,nz,"fig/comparison{0}3_projection4-5.png".format(i))

			disp.display_matrix(MatrixS,mypoint,mypointw,alphas,betas,betasy,alphasx,valuesW,"comparison{0}3".format(i))
	elif typeOpt == 9: # ioc and ortho tests with more noise
		#ydata = np.zeros((nz*T,6)) #traj
		funcod = init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
		funcoi = init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)

		T=len(np.arange(0,trueT+step,step))
		alphas = np.arange(30,90,0.5)#np.min(xdata)-10, np.max(xdata)+10, 0.05)
		betas = np.arange(-200,-80,1)#np.arange(np.min(ydata)-5,np.max(ydata)+5, 0.05)
		MatrixS = np.zeros((len(betas),len(alphas)))
		MatrixW = np.zeros((len(betas),len(alphas),2))
		for i in range(len(alphas)):
			count = "states : {0} / {1} ".format(i+1,len(alphas))
			print(count)
			for j in range(len(betas)):
				a = alphas[i]
				b = betas[j]
				J = ptcoi.my_coi_matrix_trunc(np.array([a,b]),degree,nz,nc,trueT,step,myMod,funcoi)
				J = J / np.linalg.norm(J)
				s = sci.svdvals(J)
				s = s / np.linalg.norm(s)
				MatrixS[j][i] = s[-1]
				new_z,new_w = pcoi.my_resolution(J,nc)
				MatrixW[j,i,:] = new_w

		some_w = np.array([[0.9,0.1],[0.5,0.5],[0.1,0.9]])
		n_noise = 5
		noise_level = 0.01
		for k in range(3):
			print('___________________________________________________________________________________')
			mypointw = np.zeros((1,2,n_noise))
			mypoint = np.zeros((1,4,n_noise))

			# Trajectory Initialisation
			idw = k
			w = some_w[idw,:]
			print('******* Initialisation *********')
			print("w : {0}".format(w / np.linalg.norm(w)))
			arg_init = ptcod.cod_optim(w / np.linalg.norm(w),nz,trueT,step,Sstart,Sgoal,degree,funcod)
			trajX_init,args = pl.eval_poly_trunc(arg_init,nz,T,trueT,step,Sstart,Sgoal,degree)
			mypointw[0,:,0] = [1,-1]
			mypoint[0,:,0] = [arg_init[0], arg_init[1], arg_init[0], arg_init[1]]
			#ydata[:,0] = trajX_init
			print("Alpha/Beta : {0}".format(arg_init))

			alphasx,betasy,valuesW = fm.read_matrices("res/matrices_tot_w.csv")
			for i in range(1,n_noise):
				print("_________________{0}______________________".format(i))
				noiseSample = np.random.normal(0, noise_level, size=nz*T)

				# Noised trajectory
				print('******* Noised *********')
				trajX_noised = trajX_init + noiseSample
				x = sf.vec2matrix(trajX_noised,nz,T)
				fm.csv_save(trajX_noised,w,T,nz,x[:,0],x[:,-1],myMod,"noised_traj{0}_{1}".format(k,i),0,0,[])

				"""arg_noised = pl.complete2trunc(pl.mypolynome(trajX_noised,nz,T,trueT,step,degree))
				#ydata[:,1] = trajX_noised
				print("Alpha/Beta : {0}".format(arg_noised))

				print("RMSE noised : {0} ".format(rmse(trajX_init,trajX_noised)))
				print("Errors polynomial : {0}".format(rmse(arg_noised,arg_init)))
				#mypointw[0,:,i] = [1,-1]
				#mypoint[0,:,i] = [arg_noised[0], arg_noised[1], arg_init[0], arg_init[1]]

				print('----- Alpha and Beta')
				diff_alph = alphasx - arg_noised[0]
				diff_bet = betasy - arg_noised[1]
				idab = np.argmin((np.abs(diff_bet)+np.abs(diff_alph))/2.0)
				w_ab = valuesW[idab,:]
				print("w : {0}".format(w_ab))
				arg_proj_ab = np.array([alphasx[idab],betasy[idab]])
				print("Alpha/Beta : {0}".format(arg_proj_ab))
				print("Errors polynomial : {0}".format(rmse(arg_proj_ab,arg_init)))"""
				"""mypointw[0,:,i] = w_ab
				mypoint[0,:,i] = [arg_proj_ab[0], arg_proj_ab[1], arg_noised[0], arg_noised[1]]"""

				"""print('----- IOC Projection')
				J = ptcoi.my_coi_matrix_trunc(arg_noised,degree,nz,nc,trueT,step,myMod,funcoi)
				J = J / np.linalg.norm(J)
				new_z,new_w = pcoi.my_resolution(J,nc)
				print("w : {0}".format(new_w))
				arg_proj_ioc = ptcod.cod_optim(new_w,nz,trueT,step,Sstart,Sgoal,degree,funcod)

				mypointw[0,:,i] = new_w
				mypoint[0,:,i] = [arg_proj_ioc[0], arg_proj_ioc[1], arg_noised[0], arg_noised[1]]
				print("Alpha/Beta : {0}".format(arg_proj_ioc))
				print("Errors polynomial : {0}".format(rmse(arg_proj_ioc,arg_init)))

			disp.display_matrix(MatrixS,mypoint,mypointw,alphas,betas,betasy,alphasx,valuesW,"comparison_proj{0}_all_01_ioc".format(idw))"""
	elif typeOpt == 10: #only cod for figure David
		for k in range(1):
			print('____________ {0} ____________'.format(k))
			for i in range(1,5):
				print('**** {0} '.format(i))
				if k==0 and i==1:
					continue
				x,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read("res/simple_cod_T{0}_w{1}_noised_traj{2}_{3}.csv".format(T,2,k,i))
				funcod = init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
				optimal_points(nz,trueT,step,Sstart,Sgoal,degree,funcod,"noised{0}_{1}".format(k,i))
	elif typeOpt == 11:
		k = 0
		print('******* Initialisation *********')
		funcodi = init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
		funcoii = init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
		w = np.array([0.9,0.1])
		w /= np.linalg.norm(w)
		print("w : {0}".format(w))
		arg_init = ptcod.cod_optim(w,nz,trueT,step,Sstart,Sgoal,degree,funcodi)
		print("Alpha/Beta : {0}".format(arg_init))
		alphasxi,betasyi,valuesWi = fm.read_matrices("res/matrices_tot_w.csv")


		print('******* NOISED *********')
		for i in range(1,5):
			print('________________________________ {0} '.format(i))
			trajX_noised,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read("res/simple_cod_T{0}_w{1}_noised_traj{2}_{3}.csv".format(T,2,k,i))
			arg_noised = pl.complete2trunc(pl.mypolynome(trajX_noised,nz,T,trueT,step,degree))
			funcod = init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)
			funcoi = init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod)

			alphasx,betasy,valuesW = fm.read_matrices("res/matrices_tot_w_noised{0}_{1}.csv".format(k,i))
			print('----- Alpha and Beta')
			diff_alph = alphasx - arg_noised[0]
			diff_bet = betasy - arg_noised[1]
			idab = np.argmin((np.abs(diff_bet)+np.abs(diff_alph))/2.0)
			w_ab = valuesW[idab,:]
			print("w : {0}".format(w_ab))
			arg_proj_ab = np.array([alphasx[idab],betasy[idab]])
			print("Alpha/Beta : {0}".format(arg_proj_ab))
			print("Errors polynomial : {0}".format(rmse(arg_proj_ab,arg_init)))
			print("Errors OMEGA : {0}".format(rmse(w_ab,w)))
			diff_alphi = alphasxi - arg_noised[0]
			diff_beti = betasyi - arg_noised[1]
			idabi = np.argmin((np.abs(diff_beti)+np.abs(diff_alphi))/2.0)
			w_abi = valuesW[idabi,:]
			print("Errors OMEGA init: {0}".format(rmse(w_abi,w)))

			print('----- IOC Projection')
			J = ptcoi.my_coi_matrix_trunc(arg_noised,degree,nz,nc,trueT,step,myMod,funcoi)
			J /= np.linalg.norm(J)
			new_z,new_w = pcoi.my_resolution(J,nc)
			print("w : {0}".format(new_w))
			arg_proj_ioc = ptcod.cod_optim(new_w,nz,trueT,step,Sstart,Sgoal,degree,funcod)
			print("Alpha/Beta : {0}".format(arg_proj_ioc))
			print("Errors polynomial : {0}".format(rmse(arg_proj_ioc,arg_init)))
			print("Errors OMEGA : {0}".format(rmse(new_w,w)))
			J = ptcoi.my_coi_matrix_trunc(arg_noised,degree,nz,nc,trueT,step,myMod,funcoii)
			J /= np.linalg.norm(J)
			new_z,new_w = pcoi.my_resolution(J,nc)
			print("Errors OMEGA init: {0}".format(rmse(new_w,w)))
	elif typeOpt == 12:

		"""saveName = "026"
		print(w / np.linalg.norm(w))
		x = simple_cod(nz,T,Sstart,Sgoal,w,myMod,saveName)"""

		x,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read("res/simple_cod_T101_w2_026.csv")
		w /= np.linalg.norm(w)
		print(w)
		xt = x.reshape((T,nz))
		Sstart = xt[0,:]
		Sgoal = xt[T-1,:]
		funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod))
		test = [funGradTau]
		print("COI starting...")
		J = coi.my_coi_matrix(x,nz,nc,T,Sstart,Sgoal,myMod,test)
		start = time.time()
		new_z,new_wm = coi.my_resolution(J,nc)
		print_time_elapsed(start)
		print(new_wm)
		start = time.time()
		new_z,new_wc = coi.classic_resolution(J,nc)
		print_time_elapsed(start)
		print(new_wc)
		print("COI done")
		print("My resolution : {0}".format(rmse(new_wm,w)))
		print("Classic resolution : {0}".format(rmse(new_wc,w)))





if __name__ == '__main__':
	main(sys.argv)
