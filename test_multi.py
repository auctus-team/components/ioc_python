import sys
import time
import csv

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import utils.polynomialFunction as pl

import optimize.simpleCOD as cod
import optimize.simpleCOI as coi
import optimize.polyCOD as pcod
import optimize.polyCOI as pcoi
import optimize.polyCODtrunc as ptcod
import optimize.polyCOItrunc as ptcoi
import optimize.costFunction as cf

import math as m
import numpy as np
import sympy as sy
import scipy.linalg as sci
import scipy.integrate as integrate

from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt

def print_time_elapsed(start):
	end = time.time()
	hours, rem = divmod(end-start, 3600)
	minutes, seconds = divmod(rem, 60)
	print("Time elapsed : {:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
	return

def rmse(a,b):
	tmp = np.square(np.subtract(a,b))
	res = m.sqrt(np.sum(tmp)/len(a))
	return res

def mydeltapolynome(delta):
	f_delta = lambda x: delta*((-x**3)+(3.0*x**4)+(-3.0*x**5)+(x**6))
	y, err = integrate.quad(f_delta, 0, 1)
	return y

# Complete test :
def simple_cod(nz,T,Sstart,Sgoal,w,myMod,saveName):
	start = time.time()
	x = cod.simpleCOD_tau(nz,T,Sstart,Sgoal,w,myMod,saveName)
	print_time_elapsed(start)
	return x

def simple_coi(x,nz,nc,T,myMod):
	start = time.time()
	new_z,new_w = coi.simpleCOI_tau(x,nz,nc,T,myMod)
	print_time_elapsed(start)
	return new_w

def poly_cod(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName):
	start = time.time()
	myarg, realx = pcod.polyCOD_tau(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
	print_time_elapsed(start)
	return myarg,realx

def poly_cod_trunc(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName):
	start = time.time()
	myarg, realx, fullarg = ptcod.polyCODtrunc_tau(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
	print_time_elapsed(start)
	return myarg,realx, fullarg

def poly_coi(myarg,degree,nz,nc,trueT,step,myMod):
	start = time.time()
	new_z,new_w = pcoi.polyCOI_tau(myarg,degree,nz,nc,trueT,step,myMod)
	print_time_elapsed(start)
	return new_w

def poly_coi_trunc(myarg,x,degree,nz,nc,trueT,step,myMod):
	start = time.time()
	new_w,sL = ptcoi.polyCOItrunc_tau(myarg,x,degree,nz,nc,trueT,step,myMod)
	print_time_elapsed(start)
	return new_w,sL


# Initialize functions for test loops
def init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod):
	funTau = autowrap(cf.function4poly(cf.symb_tau(),myMod,degree,2,nz,trueT,Sstart,Sgoal))
	print("Autowrap done")
	return funTau

def init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod):
	funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
	print("autowrap done")
	return funGradTau

def init_coi(nz,trueT,degree,Sstart,Sgoal,myMod):
	funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,1,degree))
	print("autowrap done")
	return funGradTau

def optimal_points(nz,trueT,step,Sstart,Sgoal,degree,funcod,filename,optsteps=1):
	# Take between 1h30 and 2h30
	myfunw = lambda x: [np.cos(x),np.sin(x)]
	start = time.time()
	totang = np.arange(1,90,optsteps)
	xdata = np.zeros(len(totang))
	ydata = np.zeros(len(totang))
	valuesW = np.zeros((len(totang),2))
	for i in range(len(totang)):
		print('_________')
		count = "Cod states : {0} / {1} ".format(i+1,len(totang))
		print(count)
		ang = totang[i]*m.pi/(2*90)
		w = myfunw(ang)
		w = w / np.linalg.norm(w)
		if i==0:
			ptarg = ptcod.cod_optim(w,nz,trueT,step,Sstart,Sgoal,degree,funcod)
		else:
			ptarg = ptcod.cod_optim(w,nz,trueT,step,Sstart,Sgoal,degree,funcod,ptarg)
		xdata[i] = ptarg[0]
		ydata[i] = ptarg[1]
		valuesW[i,:] = w
	print_time_elapsed(start)
	fm.save_matrices(ydata,xdata,valuesW,"tot_w_{0}".format(filename))

def main(myarg):
	if len(myarg)==1:
		print("***********************")
		print("At least one argument is missing:")
		print("> python3 test_cod mode [filename]")
		print("mode: int \n 1: simple COD,\n 2: poly COD,\n 3: simple COD-polyCOI,\n 4: reading file)")
		print("***********************")
		return 0
	typeOpt=int(myarg[1])
	nz = 6;	nu = 0
	T = 101
	Sstart = [0,0,0,0,0,0];	Sgoal = [120*m.pi/180,40*m.pi/180,0,0,0,0]
	#myMod = [1,1.5,1,1.2,0.5,0.6,0.5,0.7]
	myMod = [1,1,1,1,0.5,0.5,0.5,0.5]
	w = [0.7,0.3]
	nc = len(w)
	degree = 6
	trueT = 1; step = 0.01

	if typeOpt == 1: #COD + COI simple
		saveName = "010"
		print(w / np.linalg.norm(w))
		x = simple_cod(nz,T,Sstart,Sgoal,w,myMod,saveName)
		new_w = simple_coi(x,nz,nc,T,myMod)
	elif typeOpt == 2: # poly cod/coi
		saveName = "020"
		myarg,realx = poly_cod(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
		print(myarg)
		new_w = poly_coi(myarg,degree,nz,nc,trueT,step,myMod)
	elif typeOpt == 3: #poly trunc cod/coi
		saveName = "030"
		myarg,realx, fullarg = poly_cod_trunc(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName)
		new_w = poly_coi_trunc(myarg,realx,degree,nz,nc,trueT,step,myMod)
	# Multi functions
	if typeOpt == 4: #COD + COI simple multi
		saveName = "100"
		w = [0.0006,0.0004]#,0.004,0.002,0.08,0.04,0.1,0.1]
		w /= np.linalg.norm(w)
		nc = len(w)
		print(w)
		costTau = autowrap(cf.function2cost(cf.symb2function(cf.symb_tau(),myMod)))
		costAngulPower = autowrap(cf.function2cost(cf.symb2function(cf.symb_angulPower(),myMod)))
		costAngle = autowrap(cf.function2cost(cf.symb2function(cf.symb_angle(),myMod)))
		costVel = autowrap(cf.function2cost(cf.symb2function(cf.symb_vel(),myMod)))
		costAcc = autowrap(cf.function2cost(cf.symb2function(cf.symb_acc(),myMod)))
		functions = [costAngulPower]#, costAcc, costVel, costAngle]
		x = cod.simpleCOD_multi(nz,T,Sstart,Sgoal,w,functions,myMod,saveName)

		GradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod))
		GradAngulPower = autowrap(cf.gradCost(cf.symb_angulPower(),myMod))
		GradAngle = autowrap(cf.gradCost(cf.symb_angle(),myMod))
		GradVel = autowrap(cf.gradCost(cf.symb_vel(),myMod))
		GradAcc = autowrap(cf.gradCost(cf.symb_acc(),myMod))
		Gradfunctions = [GradAngulPower]#, GradAcc, GradVel, GradAngle]
		new_w = coi.simpleCOI_multi(x,nz,nc,T,myMod,Gradfunctions)
		print("w = {0} \n new_w = {1}".format(w,new_w))
		print("RMSE w: {0}".format(rmse(w,new_w)))
	elif typeOpt == 5: # poly cod/coi multi
		saveName = "200"
		w = [0.0004,0.0006]#,0.005,0.001]#,0.7,0.7]#,0.004,0.002,0.08,0.04,0.1,0.1]
		w /= np.linalg.norm(w)
		nc = len(w)
		print(w)
		costTau = autowrap(cf.function4poly(cf.symb_tau(),myMod,degree))
		costAngulPower = autowrap(cf.function4poly(cf.symb_angulPower(),myMod,degree))
		costAngle = autowrap(cf.function4poly(cf.symb_angle(),myMod,degree))
		costVel = autowrap(cf.function4poly(cf.symb_vel(),myMod,degree))
		costAcc = autowrap(cf.function4poly(cf.symb_acc(),myMod,degree))
		functions = [costAcc]#, costAcc, costVel, costAngle]
		parg, realx = pcod.polyCOD_multi(nz,trueT,step,degree,Sstart,Sgoal,w,functions,myMod,saveName)

		GradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,1,degree))
		GradAngulPower = autowrap(cf.gradCost(cf.symb_angulPower(),myMod,1,degree))
		GradAngle = autowrap(cf.gradCost(cf.symb_angle(),myMod,1,degree))
		GradVel = autowrap(cf.gradCost(cf.symb_vel(),myMod,1,degree))
		GradAcc = autowrap(cf.gradCost(cf.symb_acc(),myMod,1,degree))
		Gradfunctions = [GradAcc]#, GradAcc, GradVel, GradAngle]
		new_z, new_w = pcoi.polyCOI_multi(parg,degree,nz,2,trueT,step,myMod,Gradfunctions)
		print("w = {0} \n new_w = {1}".format(w,new_w))
		#print("RMSE w: {0}".format(rmse(w,new_w)))
	elif typeOpt == 6: #poly trunc cod/coi multi
		saveName = "300"
		degree = 7
		print(degree)
		w = [0.000007,0.00003,0.007,0.003]
		w /= np.linalg.norm(w)
		nc = len(w)
		print(w)
		funTau = autowrap(cf.function4poly(cf.symb_tau(),myMod,degree,2,nz,trueT,Sstart,Sgoal))
		funAngulPower = autowrap(cf.function4poly(cf.symb_acc(),myMod,degree,2,nz,trueT,Sstart,Sgoal))
		functions = [funTau,funAngulPower]
		parg, realx, argpoly = ptcod.polyCODtrunc_multi(nz,trueT,step,degree,Sstart,Sgoal,w,functions,myMod,saveName)
		print('Arguments = {0}:'.format(parg))

		GradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		GradAngulPower = autowrap(cf.gradCost(cf.symb_acc(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		Gradfunctions = [GradTau,GradAngulPower]
		new_w, sigmaL = ptcoi.polyCOItrunc_multi(parg,degree,nz,nc,trueT,step,myMod,Gradfunctions)
		print("RMSE w: {0}".format(rmse(new_w,w)))
	elif typeOpt == 7:
		GradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		GradAngulPower = autowrap(cf.gradCost(cf.symb_angulPower(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		Gradfunctions = [GradTau,GradAngulPower]
		print("autowrap done")
		mypointa = []
		mypointb = []
		alphas = np.arange(-100,100,0.25)
		betas = np.arange(-100,100,0.25)
		for a in range(len(alphas)):
			print('Loading : {0} / {1}'.format(a+1,len(alphas)))
			for b in range(len(betas)):
				mya = np.array([alphas[a],betas[b]])
				J = ptcoi.my_coi_matrix_trunc(mya,nz,trueT,step,myMod,Gradfunctions)
				J = np.transpose(J).dot(J)
				#J /= np.linalg.norm(J)
				s = sci.svdvals(J)
				s /= np.linalg.norm(s)
				print(s)
				if s[-1] < 1e-3:
					mypointa = np.append(mypointa,alphas[a])
					mypointb = np.append(mypointb,betas[b])
		valueW = np.zeros((len(mypointa),2))
		print('Points found : {0}'.format(len(mypointa)))
		disp.display_matrix(np.array([]),np.array([]),np.array([]),None,None,mypointb,mypointa,valueW,"determinent_tauAngPower")
	elif typeOpt == 8:
		funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		Gradfunctions = [funGradTau]
		mypointw = np.zeros((4,2,2))
		mypoint = np.zeros((4,4,2))


		for k in range(2):
			if k==0:
				arginit,x = poly_cod(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,None)
			else:
				w = [0.3,0.7]
				arginit,x = poly_cod(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,None)
			print(pl.complete2trunc(arginit))
			xinit = x

			"""filename = 'res/simple_cod_T101_w2_010.csv'
			x,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read(filename)"""
			np.random.seed(2345)
			noiseSample = np.random.normal(0, 0.05, size=nz*T)
			x = x + noiseSample

			arg1 = pl.mypolynome(x,nz,T,trueT,step,degree)
			print(pl.complete2trunc(arg1))
			J = ptcoi.my_coi_matrix_trunc(pl.complete2trunc(arg1),nz,trueT,step,myMod,Gradfunctions)
			s = sci.svdvals(J)
			s = s / np.linalg.norm(s)
			print(s)
			X1 = pl.eval_poly(arg1,nz,T,trueT,step,degree)

			X1b,o = pl.eval_poly_trunc(pl.complete2trunc(arg1),nz,T,trueT,step,Sstart,Sgoal,degree)
			print(np.mean(sf.vec2matrix(X1,nz,T)-sf.vec2matrix(X1b,nz,T),0))
			new_z,new_w1 = pcoi.my_resolution(J,nc)

			print(Sstart)
			print(Sgoal)
			interpCost = pl.interpolation_function_trunc(x,nz,pl.complete2trunc(arg1),trueT,step,degree,myMod,Gradfunctions)
			print("InterpCost = {0}".format(interpCost))
			interpCost = pl.interpolation_function_trunc(x,nz,pl.complete2trunc(arg1),trueT,step,degree,myMod,Gradfunctions,det=1)
			print("InterpCost Det = {0}".format(interpCost))

			print('____________')
			print('Sans determinant')
			start = time.time()
			arg2 = pl.constrained_interpolation_trunc(x,nz,T,trueT,step,degree,myMod,Gradfunctions)
			print_time_elapsed(start)
			print(arg2)
			arg2 = arg2.x
			J = ptcoi.my_coi_matrix_trunc(arg2,nz,trueT,step,myMod,Gradfunctions)
			s = sci.svdvals(J)
			s = s / np.linalg.norm(s)
			print(s)
			new_z,new_w2 = pcoi.my_resolution(J,nc)
			X2,o = pl.eval_poly_trunc(arg2,nz,T,trueT,step,Sstart,Sgoal,degree)

			interpCost = pl.interpolation_function_trunc(sf.vec2matrix(x,nz,T),nz,arg2,trueT,step,degree,myMod,Gradfunctions)
			print("InterpCost = {0}".format(interpCost))
			interpCost = pl.interpolation_function_trunc(sf.vec2matrix(x,nz,T),nz,arg2,trueT,step,degree,myMod,Gradfunctions,det=1)
			print("InterpCost Det = {0}".format(interpCost))

			print('____________')
			print('Avec determinant (1)')
			start = time.time()
			arg3 = pl.constrained_interpolation_trunc(x,nz,T,trueT,step,degree,myMod,Gradfunctions,det=1)
			print_time_elapsed(start)
			print(arg3)
			arg3 = arg3.x
			J = ptcoi.my_coi_matrix_trunc(arg3,nz,trueT,step,myMod,Gradfunctions)
			s = sci.svdvals(J)
			s = s / np.linalg.norm(s)
			print(s)
			new_z,new_w3 = pcoi.my_resolution(J,nc)
			X3,o = pl.eval_poly_trunc(arg3,nz,T,trueT,step,Sstart,Sgoal,degree)

			interpCost = pl.interpolation_function_trunc(sf.vec2matrix(x,nz,T),nz,arg3,trueT,step,degree,myMod,Gradfunctions)
			print("InterpCost = {0}".format(interpCost))
			interpCost = pl.interpolation_function_trunc(sf.vec2matrix(x,nz,T),nz,arg3,trueT,step,degree,myMod,Gradfunctions,det=1)
			print("InterpCost Det = {0}".format(interpCost))

			print('____________')
			print('Avec determinant (100)')
			start = time.time()
			arg4 = pl.constrained_interpolation_trunc(x,nz,T,trueT,step,degree,myMod,Gradfunctions,det=10)
			print_time_elapsed(start)
			print(arg3)
			arg4 = arg4.x
			J = ptcoi.my_coi_matrix_trunc(arg4,nz,trueT,step,myMod,Gradfunctions)
			s = sci.svdvals(J)
			s = s / np.linalg.norm(s)
			print(s)
			new_z,new_w4 = pcoi.my_resolution(J,nc)
			X4,o = pl.eval_poly_trunc(arg4,nz,T,trueT,step,Sstart,Sgoal,degree)

			interpCost = pl.interpolation_function_trunc(sf.vec2matrix(x,nz,T),nz,arg4,trueT,step,degree,myMod,Gradfunctions)
			print("InterpCost = {0}".format(interpCost))
			interpCost = pl.interpolation_function_trunc(sf.vec2matrix(x,nz,T),nz,arg4,trueT,step,degree,myMod,Gradfunctions,det=10)
			print("InterpCost Det = {0}".format(interpCost))

			print('-----------')
			print('RMSE arg1/xinit : {0}'.format(rmse(xinit,X1b)))
			print('RMSE arg2/xinit : {0}'.format(rmse(xinit,X2)))
			print('RMSE arg3/xinit : {0}'.format(rmse(xinit,X3)))
			print('RMSE arg4/xinit : {0}'.format(rmse(xinit,X4)))
			print('-----------')
			print('RMSE arg1/arginit : {0}'.format(rmse(pl.complete2trunc(arginit),pl.complete2trunc(arg1))))
			print('RMSE arg2/arginit : {0}'.format(rmse(pl.complete2trunc(arginit),arg2)))
			print('RMSE arg3/arginit : {0}'.format(rmse(pl.complete2trunc(arginit),arg3)))
			print('RMSE arg4/arginit : {0}'.format(rmse(pl.complete2trunc(arginit),arg4)))

			arginit = pl.complete2trunc(arginit)
			"""mypointw[0,:,0] = w
			mypoint[0,:,0] = [arginit[0], arginit[1], arginit[0], arginit[1]]"""
			mypointw[0,:,k] = new_w1
			arg1 = pl.complete2trunc(arg1)
			mypoint[0,:,k] = [arg1[0], arg1[1], arginit[0], arginit[1]]

			mypointw[1,:,k] = new_w2
			mypoint[1,:,k] = [arg2[0], arg2[1], arginit[0], arginit[1]]

			mypointw[2,:,k] = new_w3
			mypoint[2,:,k] = [arg3[0], arg3[1], arginit[0], arginit[1]]

			mypointw[3,:,k] = new_w4
			mypoint[3,:,k] = [arg4[0], arg4[1], arginit[0], arginit[1]]

			print('-----------')
			print('RMSE w1/winit : {0}'.format(rmse(new_w1,w)))
			print('RMSE w2/winit : {0}'.format(rmse(new_w2,w)))
			print('RMSE w3/winit : {0}'.format(rmse(new_w3,w)))
			print('RMSE w4/winit : {0}'.format(rmse(new_w4,w)))

		xdata,ydata,valuesW = fm.read_matrices("res/matrices_tot_w.csv")
		T=len(np.arange(0,trueT+step,step))
		alphas = np.arange(47,70,0.5)
		betas = np.arange(-175,-95,0.5)
		MatrixS = np.zeros((len(betas),len(alphas)))
		MatrixW = np.zeros((len(betas),len(alphas),2))
		for i in range(len(alphas)):
			count = "states : {0} / {1} ".format(i+1,len(alphas))
			print(count)
			for j in range(len(betas)):
				a = alphas[i]
				b = betas[j]
				J = ptcoi.my_coi_matrix_trunc(np.array([a,b]),nz,trueT,step,myMod,Gradfunctions)
				J = J / np.linalg.norm(J)
				s = sci.svdvals(J)
				s = s / np.linalg.norm(s)
				MatrixS[j][i] = s[-1]
				new_z,new_w = pcoi.my_resolution(J,nc)
				MatrixW[j,i,:] = new_w
		disp.display_matrix(MatrixS,mypoint,mypointw,alphas,betas,ydata,xdata,valuesW,"test_projection_matrix2")
	elif typeOpt == 9:
		print(time.asctime())
		funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		Gradfunctions = [funGradTau]
		arginit,xinit = poly_cod(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,None)
		arginit = pl.complete2trunc(arginit)
		xinitM = sf.vec2matrix(xinit,nz,T)
		w /= np.linalg.norm(w)
		Nrand = 100
		noise_level = [0,0.0001,0.0005,0.001,0.005,0.01,0.05,0.1]
		Arg_res = np.zeros((2,Nrand,len(noise_level)))
		W_res = np.zeros((2,Nrand,len(noise_level)))
		Det_res = np.zeros((2,Nrand,len(noise_level)))

		for r in range(Nrand): #random
			np.random.seed(2345+(r*112))
			for nl in range(len(noise_level)): #noise level
				start = time.time()
				noiseSample = np.random.normal(0, noise_level[nl], size=nz*T)
				x = xinit + noiseSample
				"""x = sf.vec2matrix(x,nz,T)
				x[0,:] = xinitM[0,:]
				x[-1,:] = xinitM[-1,:]
				x = sf.matrix2vec(x,nz,T)"""
				Sstart,Sgoal = sf.traj2initgoal(x,nz,T)
				funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
				Gradfunctions = [funGradTau]

				arg1 = pl.mypolynome(x,nz,T,trueT,step,degree)
				arg1 = pl.complete2trunc(arg1)
				Arg_res[0,r,nl] = rmse(arg1,arginit)
				J = ptcoi.my_coi_matrix_trunc(arg1,nz,trueT,step,myMod,Gradfunctions)
				J /= np.linalg.norm(J)
				det1 = (J[0][0]*J[1][1])-(J[1][0]*J[0][1])
				Det_res[0,r,nl] = det1
				new_z,new_w1 = pcoi.my_resolution(J,nc)
				W_res[0,r,nl] = rmse(new_w1,w)
				#X1b,o = pl.eval_poly_trunc(arg1,nz,T,trueT,step,Sstart,Sgoal,degree)

				print('...')
				arg2 = pl.constrained_interpolation_trunc(x,nz,T,trueT,step,degree,myMod,Gradfunctions,det=1)
				arg2 = arg2.x
				Arg_res[1,r,nl] = rmse(arg2,arginit)
				J = ptcoi.my_coi_matrix_trunc(arg2,nz,trueT,step,myMod,Gradfunctions)
				J /= np.linalg.norm(J)
				det2 = (J[0][0]*J[1][1])-(J[1][0]*J[0][1])
				Det_res[1,r,nl] = det2
				new_z,new_w2 = pcoi.my_resolution(J,nc)
				W_res[1,r,nl] = rmse(new_w2,w)
				"""print(w/np.linalg.norm(w))
				print(new_w2)
				print(new_w1)"""
				#X2,o = pl.eval_poly_trunc(arg2,nz,T,trueT,step,Sstart,Sgoal,degree)
				print_time_elapsed(start)
		print(time.asctime())
		filename="res/projection_interpolation_20_startgoalnoised.csv"
		with open(filename, 'w', newline='') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=',',\
				quotechar='|', quoting=csv.QUOTE_MINIMAL)
			spamwriter.writerow(['Arg1'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Arg_res[0,r,:]])
			spamwriter.writerow(['Arg2'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Arg_res[1,r,:]])
			spamwriter.writerow(['W1'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in W_res[0,r,:]])
			spamwriter.writerow(['W2'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in W_res[1,r,:]])
			spamwriter.writerow(['Det1'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Det_res[0,r,:]])
			spamwriter.writerow(['Det2'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Det_res[1,r,:]])
	elif typeOpt == 10:
		degree = 7
		print("Degree : {0}".format(degree))
		w = [0.000007,0.00003,0.007,0.003]
		w /= np.linalg.norm(w)
		nc = len(w)
		print("W : {0}".format(w))
		print(time.asctime())
		funTau = autowrap(cf.function4poly(cf.symb_tau(),myMod,degree,2,nz,trueT,Sstart,Sgoal))
		funAngulPower = autowrap(cf.function4poly(cf.symb_acc(),myMod,degree,2,nz,trueT,Sstart,Sgoal))
		functions = [funTau,funAngulPower]
		arginit, xinit, arginitcomplet = ptcod.polyCODtrunc_multi(nz,trueT,step,degree,Sstart,Sgoal,w,functions,myMod,None)
		print('Arguments = {0}:'.format(arginit))

		xinitM = sf.vec2matrix(xinit,nz,T)
		Nrand = 100
		noise_level = [0,0.0001,0.0005,0.001,0.005,0.01,0.05,0.1]
		Arg_res = np.zeros((2,Nrand,len(noise_level)))
		W_res = np.zeros((2,Nrand,len(noise_level)))
		Det_res = np.zeros((2,Nrand,len(noise_level)))
		funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		funGradAngulPower = autowrap(cf.gradCost(cf.symb_acc(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
		Gradfunctions = [funGradTau,funGradAngulPower]

		for r in range(Nrand): #random
			print("Random {0}".format(r))
			np.random.seed(2345+(r*112))
			for nl in range(len(noise_level)): #noise level
				start = time.time()
				noiseSample = np.random.normal(0, noise_level[nl], size=nz*T)
				x = xinit + noiseSample
				x = sf.vec2matrix(x,nz,T)
				x[0,:] = xinitM[0,:]
				x[-1,:] = xinitM[-1,:]
				x = sf.matrix2vec(x,nz,T)
				Sstart,Sgoal = sf.traj2initgoal(x,nz,T)
				"""funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
				Gradfunctions = [funGradTau]"""

				arg1 = pl.mypolynome(x,nz,T,trueT,step,degree)
				arg1 = pl.complete2trunc(arg1)
				Arg_res[0,r,nl] = rmse(arg1,arginit)
				J = ptcoi.my_coi_matrix_trunc(arg1,nz,trueT,step,myMod,Gradfunctions)
				J /= np.linalg.norm(J)
				det1 = np.linalg.det(J)
				Det_res[0,r,nl] = det1
				new_z,new_w1 = pcoi.my_resolution(J,nc)
				W_res[0,r,nl] = rmse(new_w1,w)
				#X1b,o = pl.eval_poly_trunc(arg1,nz,T,trueT,step,Sstart,Sgoal,degree)

				print('...')
				arg2 = pl.constrained_interpolation_trunc(x,nz,T,trueT,step,degree,myMod,Gradfunctions,det=10)
				arg2 = arg2.x
				Arg_res[1,r,nl] = rmse(arg2,arginit)
				J = ptcoi.my_coi_matrix_trunc(arg2,nz,trueT,step,myMod,Gradfunctions)
				J /= np.linalg.norm(J)
				det2 = np.linalg.det(J)
				Det_res[1,r,nl] = det2
				new_z,new_w2 = pcoi.my_resolution(J,nc)
				W_res[1,r,nl] = rmse(new_w2,w)
				"""print(w/np.linalg.norm(w))
				print(new_w2)
				print(new_w1)"""
				#X2,o = pl.eval_poly_trunc(arg2,nz,T,trueT,step,Sstart,Sgoal,degree)
				print_time_elapsed(start)
		print(time.asctime())
		filename="res/projection_interpolation_20_startgoalinit_multi.csv"
		with open(filename, 'w', newline='') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=',',\
				quotechar='|', quoting=csv.QUOTE_MINIMAL)
			spamwriter.writerow(['Arg1'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Arg_res[0,r,:]])
			spamwriter.writerow(['Arg2'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Arg_res[1,r,:]])
			spamwriter.writerow(['W1'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in W_res[0,r,:]])
			spamwriter.writerow(['W2'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in W_res[1,r,:]])
			spamwriter.writerow(['Det1'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Det_res[0,r,:]])
			spamwriter.writerow(['Det2'])
			for r in range(Nrand):
				spamwriter.writerow([ro for ro in Det_res[1,r,:]])
	elif typeOpt == 11:
		print(cf.symb_tau())




if __name__ == '__main__':
	main(sys.argv)
