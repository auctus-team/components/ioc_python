import time

import math as m
import numpy as np
import sympy as sy

import utils.statesFormat as sf
from cyipopt import minimize_ipopt
import scipy.linalg as sci
import jax.numpy as jnp
from jax import jit, grad, jacfwd, jacrev

DEBUG = 0


def symb_polynome(degree, valueArgs=None):
	if DEBUG:
		print('symb_polynome : {0}'.format(degree))
	t = sy.Symbol('t', real=True)
	if valueArgs is None:
		a = sy.MatrixSymbol('a', degree+1, 1)
		b = sy.MatrixSymbol('b', degree+1, 1)
	else:
		a = valueArgs[0:degree+1]
		b = valueArgs[degree+1:]
	p1 = 0
	p2 = 0
	for i in range(degree+1):
		p1 = p1 + sy.Poly(a[i]*t**i,t)
		p2 = p2 + sy.Poly(b[i]*t**i,t)
	return p1, p2

def symb_poly_function(degree, func2poly):
	if DEBUG:
		print('symb_poly_function : {0}'.format(degree))
	t1, t2, t3, t4, t5, t6 = sy.symbols('t1 t2 t3 t4 t5 t6', real=True)
	p1, p2 = symb_polynome(degree)
	dp1 = p1.diff()
	ddp1 = dp1.diff()
	dp2 = p2.diff()
	ddp2 = dp2.diff()
	values = {t1: p1, t2: p2, t3: dp1, t4: dp2, t5: ddp1, t6: ddp2}
	polyFunc = func2poly.subs(values)
	return polyFunc

def eval_poly(x,nz,T,trueT,step,degree):
	if DEBUG:
		print('eval_poly : {0}'.format(degree))
	p1, p2 = symb_polynome(degree, valueArgs=x)
	dp1 = p1.diff()
	ddp1 = dp1.diff()
	dp2 = p2.diff()
	ddp2 = dp2.diff()

	trajX = np.zeros(nz*T)
	theTime = np.arange(0,trueT+step,step)
	for k in range(0,len(theTime)):
		it = k*nz - 1
		trajX[it+1] = p1.eval(theTime[k])
		trajX[it+2] = p2.eval(theTime[k])
		trajX[it+3] = dp1.eval(theTime[k])
		trajX[it+4] = dp2.eval(theTime[k])
		trajX[it+5] = ddp1.eval(theTime[k])
		trajX[it+6] = ddp2.eval(theTime[k])
	return trajX

def eval_poly_trunc(x,nz,T,trueT,step,Sstart,Sgoal,degree, values=None):
	if DEBUG:
		print('eval_poly_trunc : {0}'.format(degree))
	if values is None:
		values = truncated_poly(nz,trueT,Sstart,Sgoal, degree)

	myarg = np.zeros((degree+1)*2)
	a0, a1, a2, a3, a4, a5, a6 = sy.symbols('a0 a1 a2 a3 a4 a5 a6', real=True)
	b0, b1, b2, b3, b4, b5, b6 = sy.symbols('b0 b1 b2 b3 b4 b5 b6', real=True)

	if degree == 6:
		myarg[0] = values[a0].subs(a6,x[0])
		myarg[1] = values[a1].subs(a6,x[0])
		myarg[2] = values[a2].subs(a6,x[0])
		myarg[3] = values[a3].subs(a6,x[0])
		myarg[4] = values[a4].subs(a6,x[0])
		myarg[5] = values[a5].subs(a6,x[0])
		myarg[6] = x[0]
		myarg[7] = values[b0].subs(b6,x[1])
		myarg[8] = values[b1].subs(b6,x[1])
		myarg[9] = values[b2].subs(b6,x[1])
		myarg[10] = values[b3].subs(b6,x[1])
		myarg[11] = values[b4].subs(b6,x[1])
		myarg[12] = values[b5].subs(b6,x[1])
		myarg[13] = x[1]
	elif degree == 7:
		a7, b7 = sy.symbols('a7 b7')
		myarg[0] = values[a0].subs({a6:x[0],a7:x[1]})
		myarg[1] = values[a1].subs({a6:x[0],a7:x[1]})
		myarg[2] = values[a2].subs({a6:x[0],a7:x[1]})
		myarg[3] = values[a3].subs({a6:x[0],a7:x[1]})
		myarg[4] = values[a4].subs({a6:x[0],a7:x[1]})
		myarg[5] = values[a5].subs({a6:x[0],a7:x[1]})
		myarg[6] = x[0]
		myarg[7] = x[1]
		myarg[8] = values[b0].subs({b6:x[2],b7:x[3]})
		myarg[9] = values[b1].subs({b6:x[2],b7:x[3]})
		myarg[10] = values[b2].subs({b6:x[2],b7:x[3]})
		myarg[11] = values[b3].subs({b6:x[2],b7:x[3]})
		myarg[12] = values[b4].subs({b6:x[2],b7:x[3]})
		myarg[13] = values[b5].subs({b6:x[2],b7:x[3]})
		myarg[14] = x[2]
		myarg[15] = x[3]

	trajX = eval_poly(myarg,nz,T,trueT,step,degree)
	return trajX, myarg

def mypolynome(x,nz,T,trueT,step,degree):
	if DEBUG:
		print('mypolynome : {0}'.format(degree))
	x = sf.vec2matrix(x,nz,T)
	y = np.arange(0,trueT+step,step)
	p = np.polyfit(y, x[:,0:2], degree)
	myarg = np.concatenate((np.flip(p[:,0]), np.flip(p[:,1])))
	return myarg

def complete2trunc(parg):
	if DEBUG:
		print('complete2trunc : {0}'.format(degree))
	ida = int(len(parg)/2)
	return np.concatenate((parg[6:ida],parg[ida+6:]))

# /!\ UNIQUEMENT POUR T=1
def matrix_poly(degree,trueT,nz):
	if DEBUG:
		print('matrix_poly : {0}'.format(degree))
	M = np.zeros((nz*2,(degree+1)*2))
	# for nz = 6
	M[0][0] = 1
	M[1][1] = 1
	M[2][2] = 2
	M[3,0:degree+1] = np.full(degree+1,1)#*trueT
	M[4,0:degree+1] = np.arange(degree+1)#*trueT
	tmp = np.arange(2,degree+1)
	tmp = np.arange(1,degree)*tmp
	M[5,2:degree+1] = tmp#*trueT
	M[6:,degree+1:] = M[0:6,0:degree+1]
	return M

def poly_constraints(x,nz,trueT,degree,Sstart,Sgoal,Bfun,poly=1):
	if DEBUG:
		print('poly_constraints : {0}'.format(degree))
	M = matrix_poly(degree,trueT,nz)
	jac = M
	if Bfun==0:
		return jac
	thelim = np.array([Sstart[0],Sstart[2],Sstart[4],Sgoal[0],Sgoal[2],Sgoal[4],Sstart[1],Sstart[3],Sstart[5],Sgoal[1],Sgoal[3],Sgoal[5]])
	if poly==2:
		thelim = np.array([[Sstart[0],Sstart[2],Sstart[4],Sgoal[0],Sgoal[2],Sgoal[4],Sstart[1],Sstart[3],Sstart[5],Sgoal[1],Sgoal[3],Sgoal[5]]])
		thelim = np.transpose(thelim)
	fun = jac @ x - thelim
	return fun

def truncated_poly(nz,trueT,Sstart,Sgoal, degree=6):
	if DEBUG:
		print('truncated_poly : {0}'.format(degree))
	tmparg = sy.MatrixSymbol('tmparg', (degree+1)*2, 1)
	a0, a1, a2, a3, a4, a5, a6 = sy.symbols('a0 a1 a2 a3 a4 a5 a6', real=True)
	b0, b1, b2, b3, b4, b5, b6 = sy.symbols('b0 b1 b2 b3 b4 b5 b6', real=True)
	if degree == 7:
		a7, b7 = sy.symbols('a7 b7')
	res = poly_constraints(tmparg,nz,trueT,degree,Sstart,Sgoal,1,2)
	if degree == 6:
		res = sy.Matrix(res).subs({tmparg[0]:a0, tmparg[1]:a1, tmparg[2]:a2, tmparg[3]:a3, tmparg[4]:a4, tmparg[5]:a5,tmparg[6]:a6, tmparg[7]:b0, tmparg[8]:b1, tmparg[9]:b2, tmparg[10]:b3, tmparg[11]:b4, tmparg[12]:b5,tmparg[13]:b6})
	elif degree == 7:
		res = sy.Matrix(res).subs({tmparg[0]:a0, tmparg[1]:a1, tmparg[2]:a2, tmparg[3]:a3, tmparg[4]:a4, tmparg[5]:a5,tmparg[6]:a6,tmparg[7]:a7, tmparg[8]:b0, tmparg[9]:b1, tmparg[10]:b2, tmparg[11]:b3, tmparg[12]:b4, tmparg[13]:b5,tmparg[14]:b6,tmparg[15]:b7})
	eq0 = sy.Eq(res[0,0])
	eq1 = sy.Eq(res[1,0])
	eq2 = sy.Eq(res[2,0])
	eq3 = sy.Eq(res[3,0])
	eq4 = sy.Eq(res[4,0])
	eq5 = sy.Eq(res[5,0])
	eq6 = sy.Eq(res[6,0])
	eq7 = sy.Eq(res[7,0])
	eq8 = sy.Eq(res[8,0])
	eq9 = sy.Eq(res[9,0])
	eq10 = sy.Eq(res[10,0])
	eq11 = sy.Eq(res[11,0])
	res = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8,eq9,eq10,eq11],[a0, a1, a2, a3, a4, a5, b0, b1, b2, b3, b4, b5])
	return res

def truncated_subs(mySymbFunction,nz,trueT,Sstart,Sgoal,degree=6):
	if DEBUG:
		print('truncated_subs : {0}'.format(degree))
	myarg = sy.MatrixSymbol('myarg', (degree+1)*2+1, 1)
	t = sy.Symbol('t')
	a0, a1, a2, a3, a4, a5, a6 = sy.symbols('a0 a1 a2 a3 a4 a5 a6', real=True)
	b0, b1, b2, b3, b4, b5, b6 = sy.symbols('b0 b1 b2 b3 b4 b5 b6', real=True)
	if degree == 7:
		a7, b7 = sy.symbols('a7 b7')

	if degree == 6:
		FunSymb = mySymbFunction.subs({myarg[0]:a0, myarg[1]:a1, myarg[2]:a2, myarg[3]:a3, myarg[4]:a4, myarg[5]:a5,myarg[6]:a6, myarg[7]:b0, myarg[8]:b1, myarg[9]:b2, myarg[10]:b3, myarg[11]:b4, myarg[12]:b5,myarg[13]:b6,myarg[14]:t})
	elif degree == 7:
		FunSymb = mySymbFunction.subs({myarg[0]:a0, myarg[1]:a1, myarg[2]:a2, myarg[3]:a3, myarg[4]:a4, myarg[5]:a5,myarg[6]:a6,myarg[7]:a7, myarg[8]:b0, myarg[9]:b1, myarg[10]:b2, myarg[11]:b3, myarg[12]:b4, myarg[13]:b5,myarg[14]:b6,myarg[15]:b7,myarg[16]:t})
	values = truncated_poly(nz,trueT,Sstart,Sgoal,degree)
	FunSymbTrunc = FunSymb.subs(values)
	if degree == 6:
		newarg = sy.MatrixSymbol('newarg', 2+1, 1)
		FunSymbTrunc = FunSymbTrunc.subs({a6:newarg[0], b6:newarg[1], t:newarg[2]})
	if degree == 7:
		newarg = sy.MatrixSymbol('newarg', 4+1, 1)
		FunSymbTrunc = FunSymbTrunc.subs({a6:newarg[0], a7:newarg[1],b6:newarg[2], b7:newarg[3], t:newarg[4]})

	return FunSymbTrunc


def interpolation_function(Xinit,nz,parg,trueT,step,degree,myMod,Gradfunctions):
	from optimize.polyCOItrunc import my_coi_matrix_trunc
	diffP1 = np.array([])
	diffP2 = np.array([])
	y = 0
	ytmp = 0
	theTime = np.arange(0,trueT+step,step)
	T = len(theTime)
	for k in range(T):
		t = theTime[k]
		p1 = 0
		p2 = 0
		for i in range(degree+1):
			p1 += parg[i]*(t**i)
			p2 += parg[degree+1+i]*(t**i)
		y += (p1-Xinit[k,0])**2
		ytmp += (p2-Xinit[k,1])**2
	y = y + ytmp
	#y = m.sqrt(y**2)
	"""J = my_coi_matrix_trunc(complete2trunc(arg),nz,trueT,step,myMod,Gradfunctions)
	s = sci.svdvals(J)
	s /= np.linalg.norm(s)
	y += np.prod(s)"""
	return y

def interpolation_derivative(x,nz,arg,trueT,step,degree,myMod,Gradfunctions):

	diffP1 = np.array([])
	diffP2 = np.array([])
	y = 0
	ytmp = 0
	theTime = np.arange(0,trueT+step,step)
	T = len(theTime)
	for k in range(T):
		t = theTime[k]
		p1 = 0
		p2 = 0
		for i in range(degree+1):
			p1 += arg[i]*(t**i)
			p2 += arg[degree+1+i]*(t**i)
		y += (2*p1-2*x[k,0])
		ytmp += (2*p2-2*x[k,1])
	y = y + ytmp
	return y

def interpolation_constraints(x,arg,degree,Bfun):
	M = np.zeros((4,(degree+1)*2))
	M[0][0] = 1
	M[1,0:degree+1] = np.full(degree+1,1)
	M[2][degree+1] = 1
	M[3,degree+1:] = np.full(degree+1,1)
	if Bfun==0:
		return M
	fun = M @ arg - np.array([x[0][0],x[0][-1],x[1][0],x[1][-1]])
	return fun

def constrained_interpolation(Xinit,nz,T,trueT,step,degree,myMod,Gradfunctions,arg0=None):
	if arg0 is None:
		arg0 = np.zeros((degree+1)*2)
	Xinit = sf.vec2matrix(Xinit,nz,T)
	myfun = lambda x: interpolation_function(Xinit,nz,x,trueT,step,degree,myMod,Gradfunctions)
	myfun = jit(myfun)
	obj_grad = jit(grad(myfun))  # objective gradient
	#obj_hess = jit(jacrev(jacfwd(myfun)))  # objective hessian

	cons = ({'type': 'eq',\
		'fun': lambda x:  interpolation_constraints(Xinit,x,degree,1),\
		'jac': lambda x: interpolation_constraints(Xinit,x,degree,0)})
	res = minimize_ipopt(myfun, jac=obj_grad, x0=arg0,options={'disp': 5,'max_iter': 3000})#, constraints=cons)

	"""arg0 = np.zeros((degree+1))
	Xinit = sf.vec2matrix(Xinit,nz,T)
	fun = lambda x: interpolation_function(Xinit,nz,x,trueT,step,degree,myMod,Gradfunctions)
	res = minimize_ipopt(fun, x0=arg0)
	"""
	return res

def interpolation_constraints_trunc(x,nz,trueT,step,myMod,Gradfunctions):
	from optimize.polyCOItrunc import my_coi_matrix_trunc
	J = my_coi_matrix_trunc(x,nz,trueT,step,myMod,Gradfunctions)
	s = sci.svdvals(J)
	s /= np.linalg.norm(s)
	y = 1e-7 - np.prod(s)
	return y


def interpolation_function_trunc(Xinit,nz,parg,trueT,step,degree,myMod,Gradfunctions,det=None,values=None):
	from optimize.polyCOItrunc import my_coi_matrix_trunc
	theTime = np.arange(0,trueT+step,step)
	T = len(theTime)
	Sstart,Sgoal = sf.traj2initgoal(sf.matrix2vec(Xinit,nz,T),nz,T)
	Xtest,myargs = eval_poly_trunc(parg,nz,T,trueT,step,Sstart,Sgoal,degree,values)
	Xtest = sf.vec2matrix(Xtest,nz,T)
	Xinit = sf.vec2matrix(Xinit,nz,T)
	y = 0
	ytmp = 0
	for k in range(T):
		y += (1/T)*(Xtest[k,0]-Xinit[k,0])**2
		ytmp += (1/T)*(Xtest[k,1]-Xinit[k,1])**2
	y = y + ytmp
	if det is None:
		return y
	else:
		J = my_coi_matrix_trunc(parg,nz,trueT,step,myMod,Gradfunctions)
		s = sci.svdvals(J)
		s /= np.linalg.norm(s)
		y += det*np.prod(s)**2
		#detJ = J[0][0]*J[1][1]-J[0][1]*J[1][0]
		#y += detJ**2
		return y

def constrained_interpolation_trunc(Xinit,nz,T,trueT,step,degree,myMod,Gradfunctions,arg0=None,det=None):
	if arg0 is None:
		arg0 = np.zeros(2*len(Gradfunctions))
	Sstart,Sgoal = sf.traj2initgoal(Xinit,nz,T)
	Xinit = sf.vec2matrix(Xinit,nz,T)
	values = truncated_poly(nz,trueT,Sstart,Sgoal, degree)
	myfun = lambda x: interpolation_function_trunc(Xinit,nz,x,trueT,step,degree,myMod,Gradfunctions,det,values)
	#myfun = jit(myfun)
	"""cons = ({'type': 'ineq',\
		'fun': lambda x:  interpolation_constraints_trunc(x,nz,trueT,step,myMod,Gradfunctions)})"""

	res = minimize_ipopt(myfun, x0=arg0)#, options={'disp': 5,'max_iter': 3000})#, constraints=cons)

	"""arg0 = np.zeros((degree+1))
	Xinit = sf.vec2matrix(Xinit,nz,T)
	fun = lambda x: interpolation_function(Xinit,nz,x,trueT,step,degree,myMod,Gradfunctions)
	res = minimize_ipopt(fun, x0=arg0)
"""
	return res