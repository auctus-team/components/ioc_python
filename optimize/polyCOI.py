import math as m
import numpy as np
import sympy as sy
import scipy.linalg as sci
from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import optimize.polyCOD as cod
import optimize.costFunction as cf
import utils.polynomialFunction as pl


def gradColEval(x,trueT,step,degree,nz,funGrad):
	gradCol = np.zeros((len(x),2*len(funGrad)))
	t = np.arange(0,trueT+step,step)
	myarg = np.append(x,0)
	for k in range(0,len(t)):
		myarg[len(myarg)-1] = t[k]
		for i in range(len(funGrad)):
			tmpCost = funGrad[i]
			tmpfun = tmpCost(myarg)
			for p in range(len(x)):
				gradCol[p,(i*2)+0] = gradCol[p,(i*2)+0]+tmpfun[p][0]
				gradCol[p,(i*2)+1] = gradCol[p,(i*2)+1]+tmpfun[p][1]
	return gradCol

def my_resolution(M,nc):
	u, s, vh = sci.svd(M)
	z = vh[:][np.shape(vh)[1]-1]
	ind = np.argmax(z[0:nc])
	w = np.sign(z[ind])*z[0:nc]
	w = w/np.linalg.norm(w)
	return z, w

def submatrices(M,nc):
	Jw = M[:,0:nc]
	Jr = M[:,nc:]
	return Jw,Jr

def my_coi_matrix(x,degree,nz,nc,trueT,step,myMod,funGradTau):
	jacCost = gradColEval(x,trueT,step,degree,nz,funGradTau)
	jacCons = pl.poly_constraints(x,nz,trueT,degree,[],[],0)
	jacCons = np.transpose(jacCons)
	J = np.zeros(((degree+1)*2,np.shape(jacCons)[1]+nc))
	J[:,0:nc] = jacCost
	J[:,nc:] = jacCons
	return J

def polyCOI_tau(mya,degree,nz,nc,trueT,step,myMod):
	print("***** COI MODE ACTIVATE : Polynome")
	funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,1,degree))
	Gradfunctions = [funGradTau]
	print("autowrap done")
	print("COI starting...")
	J = my_coi_matrix(mya,degree,nz,nc,trueT,step,myMod,Gradfunctions)
	s = sci.svdvals(J)
	s = s / np.linalg.norm(s)
	print(s)
	print(s[-1])
	new_z,new_w = my_resolution(J,nc)
	print("COI done")
	print(new_w)
	return new_z,new_w

def polyCOI_multi(mya,degree,nz,nc,trueT,step,myMod,Gradfunctions):
	print("***** COI MODE ACTIVATE : Polynome")
	print("COI starting...")
	J = my_coi_matrix(mya,degree,nz,nc,trueT,step,myMod,Gradfunctions)
	s = sci.svdvals(J)
	s = s / np.linalg.norm(s)
	print(s)
	print(s[-1])
	new_z,new_w = my_resolution(J,nc)
	print("COI done")
	print(new_w)
	return new_z,new_w