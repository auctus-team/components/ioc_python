import math as m
import numpy as np
import sympy as sy
import scipy.linalg as sci
from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt
from jax import jit, grad, jacfwd

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import optimize.simpleCOD as cod
import optimize.costFunction as cf


def gradColEval(x,T,nz,funGrad):
	gradCol = np.zeros((T*nz,2*len(funGrad)))
	for k in range(0,T):
		it = k*nz - 1
		for i in range(len(funGrad)):
			tmpCost = funGrad[i]
			tmpCol = tmpCost(np.array([x[it+1],x[it+2],x[it+3],x[it+4],x[it+5],x[it+6]]))
			tmpCol = np.transpose(tmpCol)
			gradCol[it+1:it+nz+1,(i*2)+0] = [tmpCol[0][0],tmpCol[0][1],tmpCol[0][2],tmpCol[0][3],tmpCol[0][4],tmpCol[0][5]]
			gradCol[it+1:it+nz+1,(i*2)+1] = [tmpCol[1][0],tmpCol[1][1],tmpCol[1][2],tmpCol[1][3],tmpCol[1][4],tmpCol[1][5]]
	return gradCol

def my_resolution(M,nc):
	u, s, vh = sci.svd(M)
	z = vh[:][np.shape(vh)[1]-1]
	ind = np.argmax(z[0:nc])
	w = np.sign(z[ind])*z[0:nc]
	w = w/np.linalg.norm(w)
	return z, w

def test_constraints(x,nc):
	#tmp = np.zeros(len(x[nc:]))
	#tmp[:] = x[nc:]
	tmp = x
	return tmp

def test_jacobian(x,nc):
	#tmp = np.zeros((len(x),len(x[nc:])))
	#tmp[nc:,:] = np.identity(len(x[nc:]))
	tmp = np.identity(len(x))
	return np.transpose(tmp)

def test_objective(M,x):
	tmp = np.linalg.norm(M @ x)**2
	#print(np.shape(tmp))
	return tmp

def test_2jacobian(x):
	tmp = np.zeros(len(x))
	if np.linalg.norm(x) == 0:
		tmp[:] = x
	else:
		tmp[:] = x / np.linalg.norm(x)
	#print("je test 2 = {0}".format(np.shape(np.transpose(tmp))))
	return tmp

def classic_resolution(M,nc):
	x0 = np.zeros(np.shape(M)[1])
	print(np.shape(test_constraints(x0,nc)))
	fun = lambda x: test_objective(M,x)
	cons = [{'type': 'eq',
		'fun': lambda x: np.linalg.norm(x)-1,
		'jac': lambda x: test_2jacobian(x)},
		{'type': 'ineq',
		'fun': lambda x: test_constraints(x,nc),
		'jac': lambda x: test_jacobian(x,nc)}]
	# HYPER IMPORTANT : les col des jacobiennes doivent etre de mm taille
	print(np.shape(M))
	print(np.shape(x0))
	res = minimize_ipopt(fun, x0,constraints=cons)#,options={'disp': 5})
	z = res.x
	ind = np.argmax(z[0:nc])
	w = np.sign(z[ind])*z[0:nc]
	w = w/np.linalg.norm(w)
	return z, w

def submatrices(M,nc):
	Jw = M[:,0:nc]
	Jr = M[:,nc:]
	return Jw,Jr

def my_coi_matrix(x,nz,nc,T,Sstart,Sgoal,myMod,funGrad):
	jacCost = gradColEval(x,T,nz,funGrad)
	jacCons = cod.my_constraints(x,nz,T,Sstart,Sgoal,0)
	jacCons = np.transpose(jacCons)
	J = np.zeros((T*nz,np.shape(jacCons)[1]+nc))
	J[:,0:nc] = jacCost
	J[:,nc:] = jacCons
	return J

def simpleCOI_tau(x,nz,nc,T,myMod):
	print("***** COI MODE ACTIVATE : simple")
	xt = x.reshape((T,nz))
	Sstart = xt[0,:]
	Sgoal = xt[T-1,:]
	funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod))
	test = [funGradTau]
	print("COI starting...")
	J = my_coi_matrix(x,nz,nc,T,Sstart,Sgoal,myMod,test)
	new_z,new_w = my_resolution(J,nc)
	print("COI done")
	print(new_w)
	return new_z,new_w

def simpleCOI_multi(x,nz,nc,T,myMod,Gradfunctions):
	print("***** COI MODE ACTIVATE : simple")
	xt = x.reshape((T,nz))
	Sstart = xt[0,:]
	Sgoal = xt[T-1,:]
	print("COI starting...")
	J = my_coi_matrix(x,nz,nc,T,Sstart,Sgoal,myMod,Gradfunctions)
	new_z,new_w = my_resolution(J,nc)
	print("COI done")
	print(new_w)
	return new_z,new_w