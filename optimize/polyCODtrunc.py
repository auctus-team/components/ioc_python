import math as m
import numpy as np
import sympy as sy
from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt

import utils.filemanager as fm
import utils.display as disp
import optimize.simpleCOD as cod
import optimize.polyCOD as pcod
import utils.statesFormat as sf
import optimize.costFunction as cf
import utils.polynomialFunction as pl

def polytrunc_optim(w,trueT,step,costFun, x0=None):
	if x0 is None:
		x0 = np.zeros(2)
	fun = lambda x: pcod.poly_optim_val(x,w,trueT,step,costFun)
	print("COD starting...")
	res = minimize_ipopt(fun, x0)
	print("COD done")
	return res.x

def polyCODtrunc_tau(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName):
	print("***** COD MODE ACTIVATE : Truncated polynome")
	w = w / np.linalg.norm(w)
	print(w)
	T=len(np.arange(0,trueT+step,step))
	funTau = autowrap(cf.function4poly(cf.symb_tau(),myMod,degree,2,nz,trueT,Sstart,Sgoal))
	print("Autowrap done")
	functions = [funTau]
	parg = polytrunc_optim(w,trueT,step,functions)
	print(parg)
	realx, argpoly = pl.eval_poly_trunc(parg,nz,T,trueT,step,Sstart,Sgoal,degree)
	myfunc = autowrap(cf.symb2function(cf.symb_tau(),myMod))
	tau,dtau,ddtau = sf.tau_eval(realx, nz, T, myfunc)
	print(argpoly)
	if saveName is None:
		print("COD data not saved")
	else:
		fm.csv_save(realx,w,T,nz,Sstart,Sgoal,myMod,saveName,1,degree,parg)
		print("COD data saved into "+saveName)
	disp.display_torque(tau,T,nz,"fig/poly_tau.png")
	disp.display_trajectory(realx,T,nz,"fig/poly_traj.png")
	return parg, realx, argpoly

def polyCODtrunc_multi(nz,trueT,step,degree,Sstart,Sgoal,w,functions,myMod,saveName):
	print("***** COD MODE ACTIVATE : Truncated polynome")
	w = w / np.linalg.norm(w)
	print(w)
	T=len(np.arange(0,trueT+step,step))
	print("Autowrap done")
	if degree == 6:
		x0 = np.zeros(2)
	elif degree ==7:
		x0 = np.zeros(4)
	parg = polytrunc_optim(w,trueT,step,functions,x0)
	print(parg)
	realx, argpoly = pl.eval_poly_trunc(parg,nz,T,trueT,step,Sstart,Sgoal,degree)
	myfunc = autowrap(cf.symb2function(cf.symb_tau(),myMod))
	tau,dtau,ddtau = sf.tau_eval(realx, nz, T, myfunc)
	print(argpoly)
	if saveName is None:
		print("COD data not saved")
	else:
		fm.csv_save(realx,w,T,nz,Sstart,Sgoal,myMod,saveName,1,degree,parg)
		print("COD data saved into "+saveName)
	disp.display_torque(tau,T,nz,"fig/poly_tau.png")
	disp.display_trajectory(realx,T,nz,"fig/poly_traj.png")
	return parg, realx, argpoly